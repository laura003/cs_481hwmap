﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace map
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void OnButtonClicked(object sender, EventArgs e)
        {
            Button button = sender as Button;
            switch (button.Text)                                         //
            {                                                            //
                case "Street":                                           //
                    map.MapType = MapType.Street;                        //
                    break;                                               //
                case "Satellite":                                        // Change Map Type
                    map.MapType = MapType.Satellite;                     //
                    break;                                               //
                case "Hybrid":                                           //
                    map.MapType = MapType.Hybrid;                        //
                    break;                                               //
            }                                                            //
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var pin = new Pin                                                                                 //
            {                                                                                                 //
                Position = new Position(48.692569, 6.188284),                                                 //
                Label = "My Epitech School",                                                                  //
                Address = "80 Rue Saint-Georges, 54000 Nancy, France"                                         //
            };                                                                                                //
                                                                                                              //
            var pin1 = new Pin                                                                                //
            {                                                                                                 //
                Position = new Position(48.693662, 6.183263),                                                 //
                Label = "Most popular place in Nancy",                                                        //
                Address = "Place Stanislas, 54000 Nancy, France"                                              //
            };                                                                                                // Create differents pins
                                                                                                              //
            var pin2 = new Pin                                                                                //
            {                                                                                                 //
                Position = new Position(43.276964, 3.503973),                                                 //
                Label = "Coolest nightclub",                                                                  //
                Address = "ÎLE DES LOISIRS, 5 Parking du temps Libre, 34300 Agde, France"                     //
            };                                                                                                //
                                                                                                              //
            var pin3 = new Pin                                                                                //
            {                                                                                                 //
                Position = new Position(29.979325, 31.134198),                                                //
                Label = "Pyramide de Khéops",                                                                 //
                Address = "Al Haram, Nazlet El-Semman, Al Giza Desert, Giza Governorate, Égypte"              //
            };                                                                                                //

            picker.Items.Add("Epitech School");                           //
            picker.Items.Add("Place Stanislas");                          // Add Picker string for selection
            picker.Items.Add("Nightclub");                                //
            picker.Items.Add("Egypte Pyramide");                          //

            map.Pins.Add(pin);          //
            map.Pins.Add(pin1);         // Add pins on the map
            map.Pins.Add(pin2);         //
            map.Pins.Add(pin3);         //
        }

        private void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            Picker picker = sender as Picker;

            switch (picker.SelectedItem.ToString())
            {
                case "Epitech School":
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(map.Pins[0].Position, Distance.FromMeters(50)));                 //
                    break;                                                                                                        //
                case "Place Stanislas":                                                                                           //
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(map.Pins[1].Position, Distance.FromMeters(50)));                 //
                    break;                                                                                                        // Zoom on the selected place 
                case "Nightclub":                                                                                                 //
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(map.Pins[2].Position, Distance.FromMeters(50)));                 //
                    break;                                                                                                        //
                case "Egypte Pyramide":                                                                                           //
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(map.Pins[3].Position, Distance.FromKilometers(1)));              //
                    break;
            }
        }
    }
}
